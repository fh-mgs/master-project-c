#include <enet/enet.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <string>
#include "common.h"
#ifdef _WIN32
#include <process.h>
#define GETPID() (_getpid())
#else
#include <unistd.h>
#define GETPID() (getpid())
#endif

void redirect(ENetPeer* peer, uint32_t clientId) {
    EntryServerRedirectMessage msg;
    msg.clientId = clientId;
    msg.host = ENET_HOST_ANY; // enet_address_set_host_ip(msg.address.host, ip)
    msg.port = Common::SERVER_PORT_START + (clientId % Common::NUMBER_SERVERS);

    ENetPacket* packet = enet_packet_create(&msg,
        sizeof(msg),
        ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send(peer, 0, packet);
}

void log_statistics(ENetHost* server) {
    LOG_INFO("################################################\n");
    LOG_INFO("################## STATISTICS ##################\n");
    LOG_INFO("################################################\n");
    LOG_INFO("connectedPeers: %zu\n", server->connectedPeers);
    LOG_INFO("totalReceivedPackets: %d\n", server->totalReceivedPackets);
    LOG_INFO("totalSentPackets: %d\n", server->totalSentPackets);
    LOG_INFO("totalReceivedData: %d\n", server->totalReceivedData);
    LOG_INFO("totalSentData: %d\n", server->totalSentData);
    enet_deinitialize();
}

void export_csv(std::string path, ENetHost* server)
{
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1); // timestamp in ms

    // file pointer
    std::fstream filestream;
    // opens an existing csv file or creates a new file.
    filestream.open(path, std::ios::out | std::ios::app);

    // Insert the data to file
    filestream << "entryserver" << ","
        << timestamp << ","
        << "" << "," // no id
        << server->connectedPeers << ","
        << server->totalReceivedPackets << ","
        << server->totalSentPackets << ","
        << server->totalReceivedData << ","
        << server->totalSentData
        << "\n";

    filestream.close();
}


int main()
{
    /* create filename of export csv */
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);  // timestamp in ms
    std::string csvPath = "exports/entryserver_" + std::to_string(GETPID()) + "_" + std::to_string(timestamp) + ".csv"; // exports/entryserver_[pid]_[timestamp].csv

    if (enet_initialize() != 0)
    {
        LOG_ERROR("An error occurred while initializing ENet.\n");
        return EXIT_FAILURE;
    }
    atexit(enet_deinitialize);

    ENetAddress address;
    ENetHost* server;
    address.host = ENET_HOST_ANY;
    address.port = 5000;
    server = enet_host_create(&address /* the address to bind the server host to */,
        ENET_PROTOCOL_MAXIMUM_PEER_ID  /* allow up to 4095 clients and/or outgoing connections */,
        1      /* allow up to 1 channels to be used, 0 */,
        0      /* assume any amount of incoming bandwidth */,
        0      /* assume any amount of outgoing bandwidth */);
    if (server == NULL)
    {
        LOG_ERROR("An error occurred while trying to create an ENet server host.\n");
        exit(EXIT_FAILURE);
    }

    uint32_t clientId = 0;
    ENetEvent event;
    /* handle events. */
    while (enet_host_service(server, &event, 15000) > 0) {
        switch (event.type)
        {
        case ENET_EVENT_TYPE_NONE:
            break;
        case ENET_EVENT_TYPE_CONNECT:
            LOG_INFO("A new client connected from %x:%u.\n",
                event.peer->address.host,
                event.peer->address.port);
            /* Store any relevant client information here. */
            //event.peer->data = const_cast<char*>("Client information");
            // redirect to real server
            redirect(event.peer, clientId++);
            break;
        case ENET_EVENT_TYPE_RECEIVE:
            LOG_INFO("A packet of length %zu containing %s was received from %d on channel %u.\n",
                event.packet->dataLength,
                event.packet->data,
                event.peer->connectID,
                event.channelID);
            /* Clean up the packet now that we're done using it. */
            enet_packet_destroy(event.packet);
            break;
        case ENET_EVENT_TYPE_DISCONNECT:
            LOG_INFO("A client disconnected from %x:%u.\n",
                event.peer->address.host,
                event.peer->address.port);
            /* Reset the peer's client information. */
            event.peer->data = nullptr;
        }
    }

    log_statistics(server);
    export_csv(csvPath, server);

    enet_host_destroy(server);
}