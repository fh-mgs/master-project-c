#pragma once

#include <enet/enet.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include "common.h"

#if !NOGUI
#include <SFML/Graphics.hpp>
#endif

class ZoningServer
{
private:
	struct NeighborZonePort {
		bool exists;
		uint16_t port = 0;
	};

	ENetHost* server;
	uint16_t serverId;
	ENetAddress address;
	bool useGUI;
	glm::vec3 serverMapStartPos;
	glm::vec3 serverMapSize;
	uint32_t numberEnemies;
	GameState gameState;
	uint32_t updateCount = 0;
	std::string csvPath;

#if !NOGUI
	// sfml
	sf::RenderWindow window;
	std::vector<sf::Vertex> serverBoundaryLines;
#endif

	// neighbor ports
	NeighborZonePort neighborZoneRight;
	NeighborZonePort neighborZoneTop;
	NeighborZonePort neighborZoneLeft;
	NeighborZonePort neighborZoneBottom;

	// connection handling
	std::vector<ENetPeer*> clients;
	std::vector<ENetPeer*> servers;

	void set_neighbor_ports(int col, int row, int cols, int rows);
	bool is_neighbor_server(uint32_t port);
	bool is_active_client(ENetPeer* peer);
	void connect_to_neighbor(const NeighborZonePort& neighbor);
	void client_broadcast(enet_uint8 channelID, ENetPacket* packet);
	void server_broadcast(enet_uint8 channelID, ENetPacket* packet);
	void handle_connect(const ENetEvent& event);
	void handle_disconnect(const ENetEvent& event);
	void handle_receive(const ENetEvent& event);
	void update_ai_enemies();
	void update();
	void render();
	void log_statistics();
	void export_csv();
	void redirect_client(ENetPeer* peer, enet_uint16 port);
	bool redirect_if_out_of_server_bounds(ENetPeer* peer, glm::vec3 playerPosition);
	glm::vec3 clamp_to_map_bounds(glm::vec3 position);
public:
	ZoningServer(uint16_t serverId, bool useGUI);
	void run();
};

