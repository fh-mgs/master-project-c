#include <iostream>
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <fstream>
#include <chrono>
#ifdef _WIN32
#include <process.h>
#define NOMINMAX
#define GETPID() (_getpid())
#else
#include <unistd.h>
#define GETPID() (getpid())
#endif
#include "instancing_server.h"

InstancingServer::InstancingServer(uint16_t serverId, bool useGUI)
	: server(NULL), serverId(serverId), useGUI(useGUI), serverMapStartPos(glm::vec3(0.0f)), serverMapSize(glm::vec3(Common::MAP_SIZE)), numberEnemies(Common::NUMBER_ENEMIES) {

    std::srand(serverId);

    address.host = ENET_HOST_ANY;
    address.port = Common::SERVER_PORT_START + serverId;

    for (auto i = Common::NUMBER_CLIENTS; i < Common::NUMBER_CLIENTS + numberEnemies; ++i) {
        EntityState entityState;
        entityState.clientId = i;
        entityState.position = serverMapStartPos + glm::linearRand(glm::vec3(0.0f), serverMapSize);

        auto randRot2d = glm::circularRand(1.0f);
        entityState.rotation = glm::vec3(randRot2d.x, 0, randRot2d.y);

        gameState.activeEntities.push_back(entityState);
    }

#if !NOGUI
    // gui configuration
    if (useGUI) {
        std::string title = "Server " + std::to_string(serverId);
        window.create(sf::VideoMode(480, 480), title.c_str());
        window.setView(sf::View(sf::FloatRect(0.0f, 0.0f, Common::MAP_SIZE + Common::RENDER_ENTITY_SIZE, Common::MAP_SIZE + Common::RENDER_ENTITY_SIZE)));
    }
#endif

}

glm::vec3 InstancingServer::clamp_to_map_bounds(glm::vec3 position) {
    return glm::clamp(position, glm::vec3(0.0f), glm::vec3(Common::MAP_SIZE));
}

void InstancingServer::handle_connect(const ENetEvent& event) {
    auto playerId = event.data;

    LOG_INFO("[%hu] Client connected - player ID: %d\n", address.port, playerId);

    /* get initial player state if player is not comming from other server
       this could be a database call in a real mmo implementation */
    EntityState initialPlayerState;
    initialPlayerState.clientId = playerId;
    initialPlayerState.commandTime = enet_time_get();
    initialPlayerState.position = serverMapStartPos + glm::linearRand(glm::vec3(0.0f), serverMapSize);

    auto randRot2d = glm::circularRand(1.0f);
    initialPlayerState.rotation = glm::vec3(randRot2d.x, 0, randRot2d.y);

    auto entityPtr = gameState.add_player(initialPlayerState);
    event.peer->data = new int32_t(entityPtr->clientId); // set client id as peer data

    /* send initial player state */
    ENetPacket* packet = enet_packet_create(&initialPlayerState, sizeof(initialPlayerState), 0);
    enet_peer_send(event.peer, 0, packet);

    /* broadcast login event */
    LoginMessage loginMsg;
    loginMsg.clientId = playerId;
    ENetPacket* loginMsgPacket = enet_packet_create(&loginMsg, sizeof(loginMsg), 0);
    enet_host_broadcast(server, 0, loginMsgPacket);

    LOG_DEBUG("[%hu] Send initial player state - player ID: %d, position: %s, rotation: %s\n", address.port, initialPlayerState.clientId, glm::to_string(initialPlayerState.position).c_str(), glm::to_string(initialPlayerState.rotation).c_str());
}

void InstancingServer::handle_disconnect(const ENetEvent& event) {
    auto playerId = *reinterpret_cast<int32_t*>(event.peer->data);

    LOG_INFO("[%hu] Client disconnected - player ID: %d\n", address.port, playerId);

    // TODO: do nothing if server disconnected

    gameState.remove_player(playerId);

    /* broadcast logout event */
    LogoutMessage logoutMsg;
    logoutMsg.clientId = playerId;
    ENetPacket* logoutMsgPacket = enet_packet_create(&logoutMsg, sizeof(logoutMsg), 0);
    enet_host_broadcast(server, 0, logoutMsgPacket);

    /* Reset the peer's client information. */
    delete reinterpret_cast<int32_t*>(event.peer->data);
    event.peer->data = NULL;

    LOG_DEBUG("[%hu] Broadcast player disconnected - player ID: %d\n", address.port, playerId);
}

void InstancingServer::handle_receive(const ENetEvent& event) {
    LOG_DEBUG("[%hu] A packet of length %zu was received from %hu on channel %hhu.\n",
        address.port,
        event.packet->dataLength,
        *reinterpret_cast<int32_t*>(event.peer->data),
        event.channelID);

    if (event.peer->data == NULL) {
        LOG_ERROR("[%hu] Receive failed because peer data is null\n", address.port);
        return;
    }

    /* deserialize data */
    MessageType messageType = static_cast<MessageType>(event.packet->data[0]);
    auto playerId = *reinterpret_cast<int32_t*>(event.peer->data);

    if (messageType == MessageType::PLAYER_MOVE) {
        auto data = reinterpret_cast<PlayerMoveMessage*>(event.packet->data);
        if (glm::length(data->direction) == 0.0f) data->direction = glm::vec3(0.0f, 0.0f, 1.0f);
        auto rotation = glm::normalize(data->direction);
        auto player = gameState.get_player(playerId);
        player->position += rotation * Common::MOVE_SPEED * Common::MS_PER_UPDATE / 1000.0f;
        player->position = clamp_to_map_bounds(player->position);
    }

    /* Clean up the packet now that we're done using it. */
    enet_packet_destroy(event.packet);
}

void InstancingServer::update_ai_enemies() {
    ++updateCount;

    for (uint32_t i = 0; i < numberEnemies; ++i) {
        if (updateCount % Common::CHANGE_AI_MOVE_ROTATION_AFTER_UPDATES == 0) {
            auto randRot2d = glm::circularRand(1.0f);
            gameState.activeEntities[i].rotation = glm::vec3(randRot2d.x, 0, randRot2d.y);
        }
        /* move player in direction, ensure that position is in map bounds */
        gameState.activeEntities[i].position += gameState.activeEntities[i].rotation * Common::MOVE_SPEED * Common::MS_PER_UPDATE / 1000.0f;
        gameState.activeEntities[i].position = glm::clamp(gameState.activeEntities[i].position, serverMapStartPos, serverMapStartPos + serverMapSize);
    }
}

void InstancingServer::update() {
    char* data = nullptr;
    size_t size = 0;
    gameState.serialize_active(data, size);
    ENetPacket* packet = enet_packet_create(data, size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);

    LOG_DEBUG("[%hu] Broadcast game state - IDs: %zu, Data Length: %zu\n", address.port, gameState.activeEntities.size(), packet->dataLength);

    enet_host_broadcast(server, 0, packet);
    enet_host_flush(server);
}

void InstancingServer::render() {
#if !NOGUI
    // check all the window's events that were triggered since the last iteration of the loop
    sf::Event event;
    while (window.pollEvent(event))
    {
        // "close requested" event: we close the window
        if (event.type == sf::Event::Closed)
            window.close();
    }

    // clear the window with white color
    window.clear(sf::Color::White);

    // draw entities
    sf::CircleShape playerShape(Common::RENDER_ENTITY_SIZE);
    sf::RectangleShape enemyShape(sf::Vector2<float>(Common::RENDER_ENTITY_SIZE, Common::RENDER_ENTITY_SIZE));
    playerShape.setFillColor(sf::Color::Black);
    enemyShape.setFillColor(sf::Color::Black);
    int32_t numClients = Common::NUMBER_CLIENTS;
    for (const auto& entity : gameState.activeEntities) {
        // player
        if (entity.clientId < numClients) {
            playerShape.setPosition(entity.position.x, Common::MAP_SIZE - entity.position.z);
            window.draw(playerShape);
        }
        // ai enemy
        else {
            enemyShape.setPosition(entity.position.x, Common::MAP_SIZE - entity.position.z);
            window.draw(enemyShape);
        }
    }

    // end the current frame
    window.display();
#endif
}

void InstancingServer::log_statistics() {
    LOG_INFO("[%hu] ################################################\n", address.port);
    LOG_INFO("[%hu] ################## STATISTICS ##################\n", address.port);
    LOG_INFO("[%hu] ################################################\n", address.port);
    LOG_INFO("[%hu] connectedPeers: %zu\n", address.port, server->connectedPeers);
    LOG_INFO("[%hu] totalReceivedPackets: %d\n", address.port, server->totalReceivedPackets);
    LOG_INFO("[%hu] totalSentPackets: %d\n", address.port, server->totalSentPackets);
    LOG_INFO("[%hu] totalReceivedData: %d\n", address.port, server->totalReceivedData);
    LOG_INFO("[%hu] totalSentData: %d\n", address.port, server->totalSentData);
    enet_deinitialize();
}

void InstancingServer::export_csv()
{
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1); // timestamp in ms

    /* opens an existing csv file or creates a new file. */
    std::fstream csvStream;
    csvStream.open(csvPath, std::ios::out | std::ios::app);

    // Insert the data to file
    csvStream << "server" << ","
        << timestamp << ","
        << serverId << ","
        << server->connectedPeers << ","
        << server->totalReceivedPackets << ","
        << server->totalSentPackets << ","
        << server->totalReceivedData << ","
        << server->totalSentData
        << "\n";

    csvStream.close();
}

void InstancingServer::run() {
    /* create filename of export csv */
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);  // timestamp in ms
    csvPath = "exports/server_" + std::to_string(GETPID()) + "_" + std::to_string(timestamp) + ".csv"; // exports/server_[pid]_[timestamp].csv

    enet_time_set(0);

    server = enet_host_create(&address /* the address to bind the server host to */,
        ENET_PROTOCOL_MAXIMUM_PEER_ID  /* allow up to 4095 clients and/or outgoing connections */,
        2      /* allow up to 2 channels to be used, 0 and 1 */,
        1250000000 /* assume 10GBit/s incoming bandwidth (Amazon EC2 M5 Instance) */,
        1250000000 /* assume 10GBit/s outgoing bandwidth (Amazon EC2 M5 Instance) */);
    if (server == NULL)
    {
        LOG_ERROR("[%hu] An error occurred while trying to create an ENet server host.\n", address.port);
        exit(EXIT_FAILURE);
    }

    ENetEvent event;
    auto prevTime = enet_time_get();
    auto lag = 0.0f;
    auto lagExport = static_cast<float>(timestamp % static_cast<uint64_t>(Common::MS_PER_STATISTICS_EXPORT)); // ensure that all servers and clients export at about the same time
    while (true) {
        auto currTime = enet_time_get();
        lag += currTime - prevTime;
        lagExport += currTime - prevTime;
        prevTime = currTime;

        /* handle events. */
        while (enet_host_service(server, &event, 0) > 0) {
            switch (event.type)
            {
            case ENET_EVENT_TYPE_NONE:
                break;
            case ENET_EVENT_TYPE_CONNECT:
                handle_connect(event);
                break;
            case ENET_EVENT_TYPE_RECEIVE:
                handle_receive(event);
                break;
            case ENET_EVENT_TYPE_DISCONNECT:
                handle_disconnect(event);
            }
        }

        /* update game logic as lag permits */
        while (lag >= Common::MS_PER_SERVER_UPDATE) {
            lag -= Common::MS_PER_SERVER_UPDATE;
            update_ai_enemies();
            update(); // update at a fixed rate each time
        }

        while (lagExport >= Common::MS_PER_STATISTICS_EXPORT) {
            lagExport -= Common::MS_PER_STATISTICS_EXPORT;
            export_csv();
        }

        // stop server if Constants.STOP_SERVER_AFTER_MILLISECONDS set
        if (currTime >= Common::STOP_SERVER_AFTER_MILLISECONDS && Common::STOP_SERVER_AFTER_MILLISECONDS > 0)
        {
            for (int32_t i = static_cast<int32_t>(server->connectedPeers); i >= 0; --i)
            {
                ENetPeer* peer = server->peers + i;
                if (peer == nullptr || peer->state != ENetPeerState::ENET_PEER_STATE_CONNECTED) continue;
            }

            break;
        }

#if !NOGUI
        if (useGUI && window.isOpen())
        {
            render();
        }
#endif
    }

    log_statistics();

    enet_host_destroy(server);
}
