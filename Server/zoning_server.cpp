#include <iostream>
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <fstream>
#include <chrono>
#include <cmath>
#include <algorithm>
#include <SFML/System.hpp>
#ifdef _WIN32
#include <process.h>
#define NOMINMAX
#define GETPID() (_getpid())
#else
#include <unistd.h>
#define GETPID() (getpid())
#endif
#include "zoning_server.h"

ZoningServer::ZoningServer(uint16_t serverId, bool useGUI)
	: server(NULL), serverId(serverId), useGUI(useGUI), numberEnemies(Common::NUMBER_ENEMIES / Common::NUMBER_SERVERS) {

    std::srand(serverId);

    address.host = ENET_HOST_ANY;
    address.port = Common::SERVER_PORT_START + serverId;

    // set server map boundaries
    int sqrtNumServers = static_cast<int>(std::sqrt(Common::NUMBER_SERVERS));

    // calculate best fitting grid (every row and column completely filled)
    int cols = 0;
    int rows = 0;
    for (int i = sqrtNumServers; i > 0; --i) {
        float floatRows = static_cast<double>(Common::NUMBER_SERVERS) / static_cast<double>(i);
        // check if fitting grid
        if (floatRows == std::trunc(floatRows)) {
            rows = static_cast<int>(floatRows);
            cols = i;
            break;
        }
    }

    auto row = serverId / cols;
    auto col = serverId % cols;

    serverMapStartPos = glm::vec3((static_cast<float>(col) / cols * Common::MAP_SIZE), 0.0f, (static_cast<float>(row) / rows * Common::MAP_SIZE));
    serverMapSize = glm::vec3((static_cast<float>(Common::MAP_SIZE) / cols), 0.0f, (static_cast<float>(Common::MAP_SIZE) / rows));

    // set initial enemy states
    for (auto i = Common::NUMBER_CLIENTS; i < Common::NUMBER_CLIENTS + numberEnemies; ++i) {
        EntityState entityState;
        entityState.clientId = i;
        entityState.position = serverMapStartPos + glm::linearRand(glm::vec3(0.0f), serverMapSize);

        auto randRot2d = glm::circularRand(1.0f);
        entityState.rotation = glm::vec3(randRot2d.x, 0.0f, randRot2d.y);

        gameState.activeEntities.push_back(entityState);
    }

#if !NOGUI
    // gui configuration
    if (useGUI) {
        std::string title = "Server " + std::to_string(serverId);
        window.create(sf::VideoMode(480, 480), title.c_str());
        window.setView(sf::View(sf::FloatRect(0.0f, 0.0f, Common::MAP_SIZE + Common::RENDER_ENTITY_SIZE, Common::MAP_SIZE + Common::RENDER_ENTITY_SIZE)));

        // zone boundaries
        float windowWidth = window.getView().getSize().x;
        float windowHeight = window.getView().getSize().y;
        float cellWidth = windowWidth / cols;
        float cellHeight = windowHeight / rows;
        for (int i = 0; i < rows; ++i) {
            serverBoundaryLines.push_back(sf::Vertex(sf::Vector2<float>(0, cellHeight * i), sf::Color::Red));
            serverBoundaryLines.push_back(sf::Vertex(sf::Vector2<float>(windowWidth, cellHeight * i), sf::Color::Red));
        }
        for (int i = 0; i < cols; ++i) {
            serverBoundaryLines.push_back(sf::Vertex(sf::Vector2<float>(cellWidth * i, 0), sf::Color::Red));
            serverBoundaryLines.push_back(sf::Vertex(sf::Vector2<float>(cellWidth * i, windowHeight), sf::Color::Red));
        }
    }
#endif

    set_neighbor_ports(col, row, cols, rows);
}

void ZoningServer::set_neighbor_ports(int col, int row, int cols, int rows) {
    // set right neighbor
    if (col + 1 < cols) {
        neighborZoneRight.port = Common::SERVER_PORT_START + row * cols + col + 1;
        neighborZoneRight.exists = true;
    }
    else {
        neighborZoneRight.exists = false;
    }
    // set top neighbor
    if (row + 1 < rows) {
        neighborZoneTop.port = Common::SERVER_PORT_START + (row + 1) * cols + col;
        neighborZoneTop.exists = true;
    }
    else {
        neighborZoneTop.exists = false;
    }
    // set left neighbor
    if (col - 1 >= 0) {
        neighborZoneLeft.port = Common::SERVER_PORT_START + row * cols + col - 1;
        neighborZoneLeft.exists = true;
    }
    else {
        neighborZoneLeft.exists = false;
    }
    // set bottom neighbor
    if (row - 1 >= 0) {
        neighborZoneBottom.port = Common::SERVER_PORT_START + (row - 1) * cols + col;
        neighborZoneBottom.exists = true;
    }
    else {
        neighborZoneBottom.exists = false;
    }
}

bool ZoningServer::is_neighbor_server(uint32_t port) {
    return
        (neighborZoneRight.exists && neighborZoneRight.port == port) ||
        (neighborZoneTop.exists && neighborZoneTop.port == port) ||
        (neighborZoneLeft.exists && neighborZoneLeft.port == port) ||
        (neighborZoneBottom.exists && neighborZoneBottom.port == port);
}

bool ZoningServer::is_active_client(ENetPeer* peer) {
    return std::find_if(std::begin(clients), std::end(clients), [peer](ENetPeer* client) { return client == peer; }) != clients.end();
}

void ZoningServer::connect_to_neighbor(const NeighborZonePort& neighbor) {
    // only connect if neighbor zone exists
    if (!neighbor.exists) return;
    // avoid duplicate connections by only connecting to greater port numbers
    if (neighbor.port <= address.port) return;

    ENetAddress address;
    /* Connect to entry server */
    enet_address_set_host(&address, "localhost");
    address.port = neighbor.port;
    /* Initiate the connection, allocating the channel. */
    auto peer = enet_host_connect(server, &address, 2, 0);
    if (peer == NULL)
    {
        LOG_ERROR("[%hu] No available peers for initiating an ENet connection.\n", address.port);
        exit(EXIT_FAILURE);
    }

    servers.push_back(peer);
}

void ZoningServer::client_broadcast(enet_uint8 channelID, ENetPacket* packet) {
    for (auto currentPeer : clients)
    {
        if (currentPeer->state != ENET_PEER_STATE_CONNECTED)
            continue;

        enet_peer_send(currentPeer, channelID, packet);
    }

    if (packet->referenceCount == 0)
        enet_packet_destroy(packet);
}

void ZoningServer::server_broadcast(enet_uint8 channelID, ENetPacket* packet) {
    for (auto currentPeer : servers)
    {
        if (currentPeer->state != ENET_PEER_STATE_CONNECTED)
            continue;

        enet_peer_send(currentPeer, channelID, packet);
    }

    if (packet->referenceCount == 0)
        enet_packet_destroy(packet);
}

void ZoningServer::redirect_client(ENetPeer* peer, enet_uint16 port) {
    if (peer->data == NULL) {
        LOG_INFO("[%hu] Redirect failed because peer data is null\n", address.port);
        return;
    }

    int32_t playerId = *reinterpret_cast<int32_t*>(peer->data);

    // client redirect message
    ServerRedirectMessage redirectMsg;
    redirectMsg.host = ENET_HOST_ANY;
    redirectMsg.port = port;

    ENetPacket* redirectPacket = enet_packet_create(&redirectMsg, sizeof(redirectMsg), ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send(peer, 1, redirectPacket);

    // server initial position message
    EntityState* entityState = gameState.get_player(playerId);

    ENetPacket* entityPacket = enet_packet_create(entityState, sizeof(EntityState), ENET_PACKET_FLAG_RELIABLE);
    for (auto serverPeer : servers) {
        if (serverPeer->address.port == port) {
            enet_peer_send(serverPeer, 1, entityPacket);
            break;
        }
    }

    LOG_INFO("[%hu] Redirect player - player ID %d to server %d\n", address.port, playerId, port);
    LOG_DEBUG("[%hu]     Position: %s\n", address.port, glm::to_string(entityState->position).c_str());

    // remove player from lists to not communicate further
    auto client = std::find_if(std::begin(clients), std::end(clients), [peer](ENetPeer* client) { return client == peer; });
    if (client != std::end(clients)) {
        clients.erase(client);
    }
}

bool ZoningServer::redirect_if_out_of_server_bounds(ENetPeer* peer, glm::vec3 playerPosition) {
    if (playerPosition.x < serverMapStartPos.x && neighborZoneLeft.exists)
        redirect_client(peer, neighborZoneLeft.port);
    else if (playerPosition.x > serverMapStartPos.x + serverMapSize.x && neighborZoneRight.exists)
        redirect_client(peer, neighborZoneRight.port);
    else if (playerPosition.z < serverMapStartPos.z && neighborZoneBottom.exists)
        redirect_client(peer, neighborZoneBottom.port);
    else if (playerPosition.z > serverMapStartPos.z + serverMapSize.z && neighborZoneTop.exists)
        redirect_client(peer, neighborZoneTop.port);
    else return false;

    return true;
}


glm::vec3 ZoningServer::clamp_to_map_bounds(glm::vec3 position) {
    return glm::clamp(position, glm::vec3(0.0f), glm::vec3(Common::MAP_SIZE));
}

void ZoningServer::handle_connect(const ENetEvent& event) {
    auto playerId = event.data;

    if (is_neighbor_server(event.peer->address.port)) {
        LOG_INFO("[%hu] Server connected - port: %hu\n", address.port, event.peer->address.port);
        servers.push_back(event.peer);
        event.peer->data = new int32_t(event.peer->address.port);
        return;
    }

    LOG_INFO("[%hu] Client connected - player ID: %d\n", address.port, playerId);
    clients.push_back(event.peer);

    /* get initial player state if player is not comming from other server
       this could be a database call in a real mmo implementation */
    EntityState initialPlayerState;
    EntityState* existingPlayerState = gameState.get_player(playerId);
    /* player state does not exist yet */
    if (existingPlayerState == nullptr) {
        initialPlayerState.clientId = playerId;
        initialPlayerState.commandTime = enet_time_get();
        initialPlayerState.position = serverMapStartPos + glm::linearRand(glm::vec3(0.0f), serverMapSize);

        auto randRot2d = glm::circularRand(1.0f);
        initialPlayerState.rotation = glm::vec3(randRot2d.x, 0.0f, randRot2d.y);

        auto entityPtr = gameState.add_player(initialPlayerState);
        event.peer->data = new int32_t(entityPtr->clientId); // set client id as peer data
    }
    /* already receiver player state from other server */
    else {
        initialPlayerState = *existingPlayerState;
        event.peer->data = new int32_t(existingPlayerState->clientId);
    }

    /* send initial player state */
    ENetPacket* packet = enet_packet_create(&initialPlayerState, sizeof(initialPlayerState), 0);
    enet_peer_send(event.peer, 0, packet);

    /* broadcast login event */
    LoginMessage loginMsg;
    loginMsg.clientId = playerId;
    ENetPacket* loginMsgPacket = enet_packet_create(&loginMsg, sizeof(loginMsg), 0);
    client_broadcast(0, loginMsgPacket);

    LOG_DEBUG("[%hu] Send initial player state - player ID: %d, position: %s, rotation: %s\n", address.port, initialPlayerState.clientId, glm::to_string(initialPlayerState.position).c_str(), glm::to_string(initialPlayerState.rotation).c_str());
}

void ZoningServer::handle_disconnect(const ENetEvent& event) {

    if (is_neighbor_server(event.peer->address.port)) {
        LOG_INFO("[%hu] Server disconnected - port %hu\n", address.port, event.peer->address.port);

        auto server = std::find_if(std::begin(servers), std::end(servers), [event](ENetPeer* server) { return server == event.peer; });

        if (server != std::end(servers)) {
            servers.erase(server);
        }

        delete reinterpret_cast<int32_t*>(event.peer->data);
        event.peer->data = NULL;
        return;
    }

    if (event.peer->data == NULL) {
        LOG_INFO("[%hu] Disconnect failed because peer data is null\n", address.port);
        return;
    }

    auto playerId = *reinterpret_cast<int32_t*>(event.peer->data);

    LOG_INFO("[%hu] Client disconnected - player ID: %d\n", address.port, playerId);

    gameState.remove_player(playerId);

    auto client = std::find_if(std::begin(clients), std::end(clients), [event](ENetPeer* client) { return client == event.peer; });

    if (client != std::end(clients)) {
        clients.erase(client);
    }

    /* broadcast logout event */
    LogoutMessage logoutMsg;
    logoutMsg.clientId = playerId;
    ENetPacket* logoutMsgPacket = enet_packet_create(&logoutMsg, sizeof(logoutMsg), 0);
    client_broadcast(0, logoutMsgPacket);

    /* Reset the peer's client information. */
    delete reinterpret_cast<int32_t*>(event.peer->data);
    event.peer->data = NULL;

    LOG_DEBUG("[%hu] Broadcast player disconnected - player ID: %d\n", address.port, playerId);
}

void ZoningServer::handle_receive(const ENetEvent& event) {
    LOG_DEBUG("[%hu] A packet of length %zu was received from %hu on channel %hhu.\n",
        address.port,
        event.packet->dataLength,
        *reinterpret_cast<int32_t*>(event.peer->data),
        event.channelID);

    if (event.peer->data == NULL) {
        LOG_INFO("[%hu] Receive failed because peer data is null\n", address.port);
        return;
    }

    /* deserialize data */
    MessageType messageType = static_cast<MessageType>(event.packet->data[0]);
    auto playerId = *reinterpret_cast<int32_t*>(event.peer->data);

    /* player move command */
    if (messageType == MessageType::PLAYER_MOVE && is_active_client(event.peer)) {
        auto player = gameState.get_player(playerId);
        auto data = reinterpret_cast<PlayerMoveMessage*>(event.packet->data);
        if (glm::length(data->direction) == 0.0f) data->direction = glm::vec3(0.0f, 0.0f, 1.0f);
        auto rotation = glm::normalize(data->direction);
        player->position += rotation * Common::MOVE_SPEED * Common::MS_PER_UPDATE / 1000.0f;
        player->position = clamp_to_map_bounds(player->position);

        if (redirect_if_out_of_server_bounds(event.peer, player->position)) {
            LOG_DEBUG("[%hu] Position: %s\n", address.port, glm::to_string(player->position).c_str());
        }
    }
    /* initial player state from neighbor servers */
    else if (messageType == MessageType::PLAYER_STATE && is_neighbor_server(event.peer->address.port)) {
        auto entity = *reinterpret_cast<EntityState*>(event.packet->data);
        gameState.add_player(entity);

        LOG_DEBUG("[%hu] Received player state for player %d from server %hu\n", address.port, entity.clientId, event.peer->address.port);
        LOG_DEBUG("[%hu]     Position: %s\n", address.port, glm::to_string(entity.position).c_str());
    }

    /* Clean up the packet now that we're done using it. */
    enet_packet_destroy(event.packet);
}

void ZoningServer::update_ai_enemies() {
    ++updateCount;

    for (uint32_t i = 0; i < numberEnemies; ++i) {
        if (updateCount % Common::CHANGE_AI_MOVE_ROTATION_AFTER_UPDATES == 0) {
            auto randRot2d = glm::circularRand(1.0f);
            gameState.activeEntities[i].rotation = glm::vec3(randRot2d.x, 0.0f, randRot2d.y);
        }
        /* move player in direction, ensure that position is in map bounds */
        gameState.activeEntities[i].position += gameState.activeEntities[i].rotation * Common::MOVE_SPEED * Common::MS_PER_UPDATE / 1000.0f;
        gameState.activeEntities[i].position = glm::clamp(gameState.activeEntities[i].position, serverMapStartPos, serverMapStartPos + serverMapSize);
    }
}

void ZoningServer::update() {
    char* data = nullptr;
    size_t size = 0;
    gameState.serialize_active(data, size);
    ENetPacket* packet = enet_packet_create(data, size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);

    LOG_DEBUG("[%hu] Broadcast game state - IDs: %zu, Data Length: %zu\n", address.port, gameState.activeEntities.size(), packet->dataLength);

    client_broadcast(0, packet);
    enet_host_flush(server);
}

void ZoningServer::render() {
#if !NOGUI
    // check all the window's events that were triggered since the last iteration of the loop
    sf::Event event;
    while (window.pollEvent(event))
    {
        // "close requested" event: we close the window
        if (event.type == sf::Event::Closed)
            window.close();
    }

    // clear the window with white color
    window.clear(sf::Color::White);

    // draw entities
    sf::CircleShape playerShape(Common::RENDER_ENTITY_SIZE);
    sf::RectangleShape enemyShape(sf::Vector2<float>(Common::RENDER_ENTITY_SIZE, Common::RENDER_ENTITY_SIZE));
    playerShape.setFillColor(sf::Color::Black);
    enemyShape.setFillColor(sf::Color::Black);
    int32_t numClients = Common::NUMBER_CLIENTS;
    for (const auto& entity : gameState.activeEntities) {
        // player
        if (entity.clientId < numClients) {
            playerShape.setPosition(entity.position.x, Common::MAP_SIZE - entity.position.z);
            window.draw(playerShape);
        }
        // ai enemy
        else {
            enemyShape.setPosition(entity.position.x, Common::MAP_SIZE - entity.position.z);
            window.draw(enemyShape);
        }
    }

    window.draw(&serverBoundaryLines[0], serverBoundaryLines.size(), sf::Lines);

    // end the current frame
    window.display();
#endif
}

void ZoningServer::log_statistics() {
    LOG_INFO("[%hu] ################################################\n", address.port);
    LOG_INFO("[%hu] ################## STATISTICS ##################\n", address.port);
    LOG_INFO("[%hu] ################################################\n", address.port);
    LOG_INFO("[%hu] connectedPeers: %zu\n", address.port, server->connectedPeers);
    LOG_INFO("[%hu] totalReceivedPackets: %d\n", address.port, server->totalReceivedPackets);
    LOG_INFO("[%hu] totalSentPackets: %d\n", address.port, server->totalSentPackets);
    LOG_INFO("[%hu] totalReceivedData: %d\n", address.port, server->totalReceivedData);
    LOG_INFO("[%hu] totalSentData: %d\n", address.port, server->totalSentData);

    enet_deinitialize();
}

void ZoningServer::export_csv()
{
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1); // timestamp in ms

    /* opens an existing csv file or creates a new file. */
    std::fstream csvStream;
    csvStream.open(csvPath, std::ios::out | std::ios::app);

    // Insert the data to file
    csvStream << "server" << ","
        << timestamp << ","
        << serverId << ","
        << server->connectedPeers << ","
        << server->totalReceivedPackets << ","
        << server->totalSentPackets << ","
        << server->totalReceivedData << ","
        << server->totalSentData
        << "\n";

    csvStream.close();
}

void ZoningServer::run() {
    /* create filename of export csv */
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);  // timestamp in ms
    csvPath = "exports/server_" + std::to_string(GETPID()) + "_" + std::to_string(timestamp) + ".csv"; // exports/server_[pid]_[timestamp].csv

    enet_time_set(0);

    server = enet_host_create(&address /* the address to bind the server host to */,
        ENET_PROTOCOL_MAXIMUM_PEER_ID  /* allow up to 4095 clients and/or outgoing connections */,
        2      /* allow up to 2 channels to be used, 0 and 1 */,
        1250000000 /* assume 10GBit/s incoming bandwidth (Amazon EC2 M5 Instance) */,
        1250000000 /* assume 10GBit/s outgoing bandwidth (Amazon EC2 M5 Instance) */);
    if (server == NULL)
    {
        LOG_ERROR("[%hu] An error occurred while trying to create an ENet server host.\n", address.port);
        exit(EXIT_FAILURE);
    }

    // connect to neighbor servers
    connect_to_neighbor(neighborZoneRight);
    connect_to_neighbor(neighborZoneTop);
    connect_to_neighbor(neighborZoneLeft);
    connect_to_neighbor(neighborZoneBottom);

    ENetEvent event;
    auto prevTime = enet_time_get();
    auto lag = 0.0f;
    auto lagExport = static_cast<float>(timestamp % static_cast<uint64_t>(Common::MS_PER_STATISTICS_EXPORT)); // ensure that all servers and clients export at about the same time
    while (true) {
        auto currTime = enet_time_get();
        lag += currTime - prevTime;
        lagExport += currTime - prevTime;
        prevTime = currTime;

        /* handle events. */
        while (enet_host_service(server, &event, 0) > 0) {
            switch (event.type)
            {
            case ENET_EVENT_TYPE_NONE:
                break;
            case ENET_EVENT_TYPE_CONNECT:
                handle_connect(event);
                break;
            case ENET_EVENT_TYPE_RECEIVE:
                handle_receive(event);
                break;
            case ENET_EVENT_TYPE_DISCONNECT:
                handle_disconnect(event);
            }
        }

        /* update game logic as lag permits */
        while (lag >= Common::MS_PER_SERVER_UPDATE) {
            lag -= Common::MS_PER_SERVER_UPDATE;
            update_ai_enemies();
            update(); // update at a fixed rate each time
        }

        while (lagExport >= Common::MS_PER_STATISTICS_EXPORT) {
            lagExport -= Common::MS_PER_STATISTICS_EXPORT;
            export_csv();
        }

        // stop server if Constants.STOP_SERVER_AFTER_MILLISECONDS set
        if (currTime >= Common::STOP_SERVER_AFTER_MILLISECONDS && Common::STOP_SERVER_AFTER_MILLISECONDS > 0)
        {
            for (int32_t i = static_cast<int32_t>(server->connectedPeers); i >= 0; --i)
            {
                ENetPeer* peer = server->peers + i;
                if (peer == nullptr || peer->state != ENetPeerState::ENET_PEER_STATE_CONNECTED) continue;
            }

            break;
        }

#if !NOGUI
        if (useGUI && window.isOpen())
        {
            render();
        }
#endif
    }

    log_statistics();

    enet_host_destroy(server);
}
