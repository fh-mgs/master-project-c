#pragma once

#include <enet/enet.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include "common.h"

#if !NOGUI
#include <SFML/Graphics.hpp>
#endif

class ReplicationServer
{
private:
	ENetHost* server;
	uint16_t serverId;
	ENetAddress address;
	bool useGUI;
	glm::vec3 serverMapStartPos;
	glm::vec3 serverMapSize;
	uint32_t numberEnemies;
	GameState gameState;
	uint32_t updateCount = 0;
	std::string csvPath;

#if !NOGUI
	// sfml
	sf::RenderWindow window;
	const size_t NUM_COLORS = 128;
	const sf::Uint32 COLORS[128] { // https://stackoverflow.com/a/20298116
		0x012C58A0, 0x008941A0, 0x1CE6FFA0, 0xFF34FFA0, 0xFF4A46A0, 0xFFFF00A0, 0x006FA6A0, 0xA30059A0,
		0xFFDBE5A0, 0x7A4900A0, 0x0000A6A0, 0x63FFACA0, 0xB79762A0, 0x004D43A0, 0x8FB0FFA0, 0x997D87A0,
		0x5A0007A0, 0x809693A0, 0xFEFFE6A0, 0x1B4400A0, 0x4FC601A0, 0x3B5DFFA0, 0x4A3B53A0, 0xFF2F80A0,
		0x61615AA0, 0xBA0900A0, 0x6B7900A0, 0x00C2A0A0, 0xFFAA92A0, 0xFF90C9A0, 0xB903AAA0, 0xD16100A0,
		0xDDEFFFA0, 0x000035A0, 0x7B4F4BA0, 0xA1C299A0, 0x300018A0, 0x0AA6D8A0, 0x013349A0, 0x00846FA0,
		0x372101A0, 0xFFB500A0, 0xC2FFEDA0, 0xA079BFA0, 0xCC0744A0, 0xC0B9B2A0, 0xC2FF99A0, 0x001E09A0,
		0x00489CA0, 0x6F0062A0, 0x0CBD66A0, 0xEEC3FFA0, 0x456D75A0, 0xB77B68A0, 0x7A87A1A0, 0x788D66A0,
		0x885578A0, 0xFAD09FA0, 0xFF8A9AA0, 0xD157A0A0, 0xBEC459A0, 0x456648A0, 0x0086EDA0, 0x886F4CA0,

		0x34362DA0, 0xB4A8BDA0, 0x00A6AAA0, 0x452C2CA0, 0x636375A0, 0xA3C8C9A0, 0xFF913FA0, 0x938A81A0,
		0x575329A0, 0x00FECFA0, 0xB05B6FA0, 0x8CD0FFA0, 0x3B9700A0, 0x04F757A0, 0xC8A1A1A0, 0x1E6E00A0,
		0x7900D7A0, 0xA77500A0, 0x6367A9A0, 0xA05837A0, 0x6B002CA0, 0x772600A0, 0xD790FFA0, 0x9B9700A0,
		0x549E79A0, 0xFFF69FA0, 0x201625A0, 0x72418FA0, 0xBC23FFA0, 0x99ADC0A0, 0x3A2465A0, 0x922329A0,
		0x5B4534A0, 0xFDE8DCA0, 0x404E55A0, 0x0089A3A0, 0xCB7E98A0, 0xA4E804A0, 0x324E72A0, 0x6A3A4CA0,
		0x83AB58A0, 0x001C1EA0, 0xD1F7CEA0, 0x004B28A0, 0xC8D0F6A0, 0xA3A489A0, 0x806C66A0, 0x222800A0,
		0xBF5650A0, 0xE83000A0, 0x66796DA0, 0xDA007CA0, 0xFF1A59A0, 0x8ADBB4A0, 0x1E0200A0, 0x5B4E51A0,
		0xC895C5A0, 0x320033A0, 0xFF6832A0, 0x66E1D3A0, 0xCFCDACA0, 0xD0AC94A0, 0x7ED379A0, 0x000000A0
	};
#endif

	// connection handling
	std::vector<ENetPeer*> clients;
	std::vector<ENetPeer*> servers;

	bool is_active_server(ENetPeer* peer);
	bool is_server(uint16_t port);
	bool is_active_client(ENetPeer* peer);
	void connect_to_servers();
	void client_broadcast(enet_uint8 channelID, ENetPacket* packet);
	void server_broadcast(enet_uint8 channelID, ENetPacket* packet);
	void handle_connect(const ENetEvent& event);
	void handle_disconnect(const ENetEvent& event);
	void handle_receive(const ENetEvent& event);
	void update_ai_enemies();
	void update();
	void render();
	void log_statistics();
	void export_csv();
	glm::vec3 clamp_to_map_bounds(glm::vec3 position);
public:
	ReplicationServer(uint16_t serverId, bool useGUI);
	void run();
};

