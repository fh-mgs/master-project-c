#pragma once

#include <enet/enet.h>
#include <glm/glm.hpp>
#include <string>
#include "common.h"

#if !NOGUI
#include <SFML/Graphics.hpp>
#endif

class InstancingServer
{
private:
	ENetHost* server;
	uint16_t serverId;
	ENetAddress address;
	bool useGUI;
	glm::vec3 serverMapStartPos;
	glm::vec3 serverMapSize;
	uint32_t numberEnemies;
	GameState gameState;
	uint32_t updateCount = 0;
	std::string csvPath;

#if !NOGUI
	// sfml
	sf::RenderWindow window;
#endif

	void handle_connect(const ENetEvent& event);
	void handle_disconnect(const ENetEvent& event);
	void handle_receive(const ENetEvent& event);
	void update_ai_enemies();
	void update();
	void render();
	void log_statistics();
	void export_csv();
	glm::vec3 clamp_to_map_bounds(glm::vec3 position);
public:
	InstancingServer(uint16_t serverId, bool useGUI);
	void run();
};

