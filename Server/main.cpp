#include <enet/enet.h>
#include <glm/glm.hpp>
#include <iostream>
#include <sstream>
#include "zoning_server.h"
#include "instancing_server.h"
#include "replication_server.h"
#include "common.h"

int main(int argc, char* argv[])
{
    enum class ServerType { ZONING, INSTANCING, REPLICATION };
    uint16_t serverId;
    ServerType serverType;
    bool useGUI = false;

    std::istringstream ss1(argv[1]);
    std::istringstream ss2(argv[2]);

    if (argc == 4 && strcmp(argv[3], "--gui") == 0) {
        useGUI = true;
    }
    if (argc < 2) {
        LOG_ERROR("Missing Arguments: ServerID Z[ONING]|I[NSTANCING]|R[EPLICATION].\n");
        return EXIT_FAILURE;
    }
    else if (argc < 3) {
        LOG_ERROR("Missing Arguments: Z[ONING]|I[NSTANCING]|R[EPLICATION].\n");
        return EXIT_FAILURE;
    }
    else if (!(ss1 >> serverId)) {
        LOG_ERROR("Argument 1 is not of type ushort.\n");
        return EXIT_FAILURE;
    }
    else {
        if (ss2.str() == "Z" || ss2.str() == "ZONING") serverType = ServerType::ZONING;
        else if (ss2.str() == "I" || ss2.str() == "INSTANCING") serverType = ServerType::INSTANCING;
        else if (ss2.str() == "R" || ss2.str() == "REPLICATION") serverType = ServerType::REPLICATION;
        else {
            LOG_ERROR("Argument 2 can only be one of these strings: Z, ZONING, I, INSTANCING, R, REPLICATION.\n");
            return EXIT_FAILURE;
        }
    }

    if (enet_initialize() != 0)
    {
        LOG_ERROR("An error occurred while initializing ENet.\n");
        return EXIT_FAILURE;
    }
    atexit(enet_deinitialize);

    if (serverType == ServerType::ZONING) {
        ZoningServer server(serverId, useGUI);
        server.run();
    }
    else if (serverType == ServerType::INSTANCING) {
        InstancingServer server(serverId, useGUI);
        server.run();
    }
    else if (serverType == ServerType::REPLICATION) {
        ReplicationServer server(serverId, useGUI);
        server.run();
    }
}
