#pragma once

#include <cstdint>
#include <glm/vec3.hpp>
#include "server_message.h"

#pragma pack(push,1)
#pragma pack(1)
struct PlayerMoveMessage {
private:
	MessageType messageType = MessageType::PLAYER_MOVE;
public:
	uint32_t commandTime;
	glm::vec3 direction;
};
#pragma pack(pop)