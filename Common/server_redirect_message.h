#pragma once

#include <cstdint>
#include "server_message.h"
#include <enet/enet.h>

#pragma pack(push,1)
#pragma pack(1)
struct ServerRedirectMessage {
private:
	MessageType messageType = MessageType::SERVER_REDIRECT;
public:
	enet_uint32 host;
	enet_uint16 port;
};
#pragma pack(pop)