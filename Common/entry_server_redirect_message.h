#pragma once

#include <cstdint>
#include "server_message.h"
#include <enet/enet.h>

#pragma pack(push,1)
#pragma pack(1)
struct EntryServerRedirectMessage {
private:
	MessageType messageType = MessageType::ENTRY_SERVER_REDIRECT;
public:
	int32_t clientId;
	enet_uint32 host;
	enet_uint16 port;
};
#pragma pack(pop)