#pragma once
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "server_message.h"
#include "entry_server_redirect_message.h"
#include "server_redirect_message.h"
#include "player_move_message.h"
#include "entity_state.h"
#include "game_state.hpp"
#include "login_message.h"
#include "logout_message.h"

#define NOGUI 0 // used to debug on wsl since wsl does not allow gui
#define LOG_LEVEL ERROR_LEVEL

#define ERROR_LEVEL 0x01
#define INFO_LEVEL  0x02
#define DEBUG_LEVEL 0x03

#if LOG_LEVEL >= ERROR_LEVEL
#define LOG_ERROR(format, ...)    fprintf(stderr, format __VA_OPT__(,) __VA_ARGS__)
#else
#define LOG_ERROR(format, ...)
#endif

#if (LOG_LEVEL >= INFO_LEVEL)
#define LOG_INFO(format, ...)     printf(format __VA_OPT__(,) __VA_ARGS__)
#else
#define LOG_INFO(format, ...)
#endif

#if LOG_LEVEL >= DEBUG_LEVEL
#define LOG_DEBUG(format, ...)    printf(format __VA_OPT__(,) __VA_ARGS__)
#else
#define LOG_DEBUG(format, ...)
#endif

namespace Common {

    const float MAP_SIZE = 400.0f; // units
    const float MS_PER_UPDATE = 1000.0f / 60.0f; // 60fps, ~16.67
    const float MS_PER_SERVER_UPDATE = 50.0f; // 20 times per second, depending on how accurate player positions need to be for the clients
    const float MS_PER_STATISTICS_EXPORT = 5000.0f; // export statistics every 5 seconds
    const float MOVE_SPEED = 1.5f; // units per second
    const uint32_t CHANGE_MOVE_ROTATION_AFTER_UPDATES = 1800; // players change rotation after 30 seconds
    const uint32_t CHANGE_AI_MOVE_ROTATION_AFTER_UPDATES = 30; // players change rotation after 3 seconds
    const uint32_t NUMBER_CLIENTS = 20;
    const uint32_t NUMBER_SERVERS = 4;
    // const float SERVER_SWAP_TIME_SECONDS = 2f; // players move between servers every 2 seconds (zoning)
    const uint16_t ENTRY_SERVER_PORT = 5000;
    const uint16_t SERVER_PORT_START = 5001;
    const uint32_t STOP_SERVER_AFTER_MILLISECONDS = 30 * 60000 + 15000; // server stops after 30 minutes and 15 seconds and shows stats, 0 to disable stopping
    const uint32_t NUMBER_ENEMIES = 200; // 1 enemy per 20x20 units
    const float RENDER_ENTITY_SIZE = 3.0f;

}
