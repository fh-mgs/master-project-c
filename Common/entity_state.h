#pragma once

#include <cstdint>
#include <glm/vec3.hpp>
#include "server_message.h"

#pragma pack(push,1)
#pragma pack(1)
struct EntityState {
private:
	MessageType messageType = MessageType::PLAYER_STATE;
public:
	int32_t clientId = -1;
	uint32_t commandTime;
	glm::vec3 position;
	glm::vec3 rotation;
};
#pragma pack(pop)