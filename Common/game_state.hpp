#pragma once

#include <cstdint>
#include <glm/vec3.hpp>
#include <vector>
#include <map>
#include <algorithm>
#include "server_message.h"
#include "entity_state.h"
#include "common.h"

struct GameState {
private:
	char* buffer = nullptr;
	std::map<uint16_t, std::vector<EntityState>> shadowEntities; // <port, entitystates>
	size_t shadowEntitiesCount = 0;

public:
	GameState() = default;

	std::vector<EntityState> activeEntities;

	// shadow entities are readonly from outside of class to ensure correct shadowEntityCount value
	const std::map<uint16_t, std::vector<EntityState>>& get_shadow_entities() const {
		return shadowEntities;
	}
	size_t get_shadow_entities_count() const {
		return shadowEntitiesCount;
	}

	// returns pointer to entity
	EntityState* add_player(const EntityState& entity) {
		activeEntities.push_back(entity);
		auto activeEntityIndex = activeEntities.size() - 1;

		return &activeEntities[activeEntityIndex];
	}

	void remove_player(int32_t playerId) {
		auto found = std::find_if(std::begin(activeEntities), std::end(activeEntities), [playerId](const EntityState& entity) { return entity.clientId == playerId; });
		// not found
		if (found == std::end(activeEntities)) return;
		// found
		activeEntities.erase(found);
	}

	// returns pointer to entity
	EntityState* get_player(int32_t playerId) {
		auto found = std::find_if(std::begin(activeEntities), std::end(activeEntities), [playerId](const EntityState& entity) { return entity.clientId == playerId; });
		// not found
		if (found == std::end(activeEntities)) return nullptr;
		// found
		return &*found;
	}

	void remove_server(uint16_t port) {
		// not found
		if (shadowEntities.find(port) == shadowEntities.end()) return;
		// found -> delete
		shadowEntitiesCount -= shadowEntities[port].size();
		shadowEntities.erase(port);
	}

	void set_shadow_entities(uint16_t port, EntityState* entities, size_t entitiesCount) {
		// decrease shadow entity count if already exists
		auto found = shadowEntities.find(port);
		if (found != shadowEntities.end()) 
			shadowEntitiesCount -= found->second.size();
		// increase shadow entity count with new value
		shadowEntitiesCount += entitiesCount;
		// set value
		shadowEntities[port] = std::vector<EntityState>(entities, entities + entitiesCount);
	}

	void serialize_active(char*& data, size_t& size) {
		delete[] buffer;
		size_t bufferSize = activeEntities.size() * sizeof(EntityState) + 1;
		buffer = new char[bufferSize];
		buffer[0] = static_cast<char>(MessageType::GAME_STATE);
		memcpy(buffer + 1, &activeEntities[0], bufferSize - 1);

		data = buffer;
		size = bufferSize;
	}

	void serialize_all(char*& data, size_t& size) {
		delete[] buffer;
		size_t bufferSize = (activeEntities.size() + shadowEntitiesCount) * sizeof(EntityState) + 1;
		buffer = new char[bufferSize];
		buffer[0] = static_cast<char>(MessageType::GAME_STATE);
		memcpy(buffer + 1, &activeEntities[0], activeEntities.size() * sizeof(EntityState));
		size_t offset = 0;
		for (const auto& shadowEntity : shadowEntities) {
			memcpy(buffer + 1 + activeEntities.size() * sizeof(EntityState) + offset, &shadowEntity.second[0], shadowEntity.second.size() * sizeof(EntityState));
			offset += shadowEntity.second.size() * sizeof(EntityState);
		}

		data = buffer;
		size = bufferSize;
	}


	~GameState() {
		delete[] buffer;
	}
	GameState(const GameState& other) = delete;
	GameState& operator=(const GameState& other) = delete;
};
