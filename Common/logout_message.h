#pragma once

#include <cstdint>
#include <glm/vec3.hpp>
#include "server_message.h"

#pragma pack(push,1)
#pragma pack(1)
struct LogoutMessage {
private:
	MessageType messageType = MessageType::PLAYER_LOGOUT;
public:
	int32_t clientId;
};
#pragma pack(pop)