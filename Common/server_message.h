#pragma once

enum class MessageType : char {
    SERVER_REDIRECT,
    ENTRY_SERVER_REDIRECT,
    PLAYER_STATE,
    PLAYER_LOGIN,
    PLAYER_LOGOUT,
    GAME_STATE,
    PLAYER_MOVE
};