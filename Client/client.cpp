#include "client.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <fstream>
#include <chrono>
#ifdef _WIN32
#include <process.h>
#define GETPID() (_getpid())
#else
#include <unistd.h>
#define GETPID() (getpid())
#endif

void Client::handle_connect(const ENetEvent& event) {
    /* Store any relevant client information here. */
    event.peer->data = &event.peer->address.port;

    /* adapt connect state */
    if (clientStatus == ClientStatus::Active)
    {
        LOG_INFO("Client connected to entry server\n");
        clientStatus = ClientStatus::ConnectedToEntryServer;
    }
    else if (clientStatus == ClientStatus::RedirectInitiated)
    {
        LOG_INFO("Client connected to server\n");
        clientStatus = ClientStatus::ConnectedToServer;
    }
    else
    {
        LOG_ERROR("Client status incorrect - Connect - %d\n", static_cast<int>(clientStatus));
    }
}

// returns false if no redirect was initiated, used to terminate simulated client in tests
bool Client::handle_disconnect(const ENetEvent& event) {
    /* adapt connect state */
    if (clientStatus == ClientStatus::RedirectInitiated)
    {
        LOG_INFO("Client disconnected from server during redirect\n");
        LOG_DEBUG("Attempting redirect to %hu\n", serverAddress.port);
        peer = enet_host_connect(client, &serverAddress, 2, playerState.clientId);
        if (peer == NULL)
        {
            LOG_ERROR("No available peers for initiating an ENet connection.\n");
            exit(EXIT_FAILURE);
        }
        return true;
    }
    else if (clientStatus == ClientStatus::ConnectedToEntryServer)
    {
        LOG_ERROR("Client disconnected from entry server\n");
        clientStatus = ClientStatus::Active;
        return false;
    }
    else if (clientStatus == ClientStatus::ConnectedToServer)
    {
        LOG_ERROR("Client disconnected from server\n");
        clientStatus = ClientStatus::Active;
        return false;
    }
    else
    {
        LOG_ERROR("Client status incorrect - Disconnect - %d\n", static_cast<int>(clientStatus));
        return false;
    }


    /* Reset the peer's client information. */
    event.peer->data = nullptr;
}

void Client::handle_receive(const ENetEvent& event) {
    LOG_DEBUG("A packet of length %zu was received from %hu on channel %hhu.\n",
        event.packet->dataLength,
        *reinterpret_cast<enet_uint16*>(event.peer->data),
        event.channelID);
    /* deserialize data */
    MessageType messageType = static_cast<MessageType>(event.packet->data[0]);
    /* server message contains client id and redirect data */
    if (messageType == MessageType::ENTRY_SERVER_REDIRECT) {
        clientStatus = ClientStatus::RedirectInitiated;
        auto data = reinterpret_cast<EntryServerRedirectMessage*>(event.packet->data);
        playerState.clientId = data->clientId;
        //serverAddress.host = data->host;
        enet_address_set_host(&serverAddress, "localhost");
        serverAddress.port = data->port;
        enet_peer_disconnect(peer, 0);
    }
    /* server message contains redirect data */
    else if (messageType == MessageType::SERVER_REDIRECT) {
        clientStatus = ClientStatus::RedirectInitiated;
        auto data = reinterpret_cast<ServerRedirectMessage*>(event.packet->data);
        //serverAddress.host = data->host;
        enet_address_set_host(&serverAddress, "localhost");
        serverAddress.port = data->port;
        enet_peer_disconnect(peer, 0);
    }
    /* get initial player state */
    else if (messageType == MessageType::PLAYER_STATE) {
        auto data = reinterpret_cast<EntityState*>(event.packet->data);
        playerState = *data;
    }
    else if (messageType == MessageType::PLAYER_LOGIN)
    {
        // Handle other player logging in
        // not done to save simulated client performance
    }
    else if (messageType == MessageType::PLAYER_LOGOUT)
    {
        // Handle other player logging out
        // not done to save simulated client performance
    }
    else if (messageType == MessageType::GAME_STATE)
    {
        // Here the client would receive all player states from the server and interpolate by the received server calculation timestamp and player state values
        // not done to save simulated client performance
    }

    /* Clean up the packet now that we're done using it. */
    enet_packet_destroy(event.packet);
}

void Client::update() {
    ++updateCount;

    /* do nothing if not connected to server */
    if (playerState.clientId < 0 || clientStatus != ClientStatus::ConnectedToServer) return;

    /* change random direction if needed */
    if (updateCount % Common::CHANGE_MOVE_ROTATION_AFTER_UPDATES == 0) {
        auto randRot2d = glm::circularRand(1.0f);
        playerState.rotation = glm::vec3(randRot2d.x, 0, randRot2d.y);
    }
    /* move player in direction, ensure that position is in map bounds */
    playerState.position += playerState.rotation * Common::MOVE_SPEED * Common::MS_PER_UPDATE / 1000.0f;
    playerState.position = glm::clamp(playerState.position, glm::vec3(0.0f), glm::vec3(Common::MAP_SIZE));

    /* send new player state to server */
    PlayerMoveMessage msg;
    msg.commandTime = enet_time_get();
    msg.direction = playerState.rotation;

    ENetPacket* packet = enet_packet_create(&msg, sizeof(msg), 0);
    enet_peer_send(peer, 0, packet);
}

void Client::log_statistics() {
    LOG_INFO("################################################\n");
    LOG_INFO("################## STATISTICS ##################\n");
    LOG_INFO("################################################\n");
    LOG_INFO("connectedPeers: %zu\n", client->connectedPeers);
    LOG_INFO("totalReceivedPackets: %d\n", client->totalReceivedPackets);
    LOG_INFO("totalSentPackets: %d\n", client->totalSentPackets);
    LOG_INFO("totalReceivedData: %d\n", client->totalReceivedData);
    LOG_INFO("totalSentData: %d\n", client->totalSentData);
    enet_deinitialize();
}

void Client::export_csv()
{
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1); // timestamp in ms

    /* opens an existing csv file or creates a new file. */
    std::fstream csvStream;
    csvStream.open(csvPath, std::ios::out | std::ios::app);

    // Insert the data to file
    csvStream << "client" << ","
        << timestamp << ","
        << playerState.clientId << ","
        << client->connectedPeers << ","
        << client->totalReceivedPackets << ","
        << client->totalSentPackets << ","
        << client->totalReceivedData << ","
        << client->totalSentData
        << "\n";

    csvStream.close();
}


void Client::run() {
    /* create filename of export csv */
    uint64_t timestamp = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);  // timestamp in ms
    csvPath = "exports/client_" + std::to_string(GETPID()) + "_" + std::to_string(timestamp) + ".csv"; // exports/client_[pid]_[timestamp].csv

    enet_time_set(0);

    client = enet_host_create(NULL /* create a client host */,
        1      /* allow up to 1 clients and/or outgoing connections */,
        2      /* allow up to 2 channels to be used, 0 and 1 */,
        0      /* assume any amount of incoming bandwidth */,
        0      /* assume any amount of outgoing bandwidth */);
    if (client == NULL)
    {
        LOG_ERROR("An error occurred while trying to create an ENet client host.\n");
        exit(EXIT_FAILURE);
    }

    ENetAddress address;
    ENetEvent event;
    /* Connect to entry server */
    enet_address_set_host(&address, "localhost");
    address.port = 5000;
    /* Initiate the connection, allocating the channel. */
    peer = enet_host_connect(client, &address, 1, 0);
    if (peer == NULL)
    {
        LOG_ERROR("No available peers for initiating an ENet connection.\n");
        exit(EXIT_FAILURE);
    }

    bool terminate = false;
    auto prevTime = enet_time_get();
    auto lag = 0.0f;
    auto lagExport = static_cast<float>(timestamp % static_cast<uint64_t>(Common::MS_PER_STATISTICS_EXPORT)); // ensure that all servers and clients export at about the same time
    while (true) {
        auto currTime = enet_time_get();
        lag += currTime - prevTime;
        lagExport += currTime - prevTime;
        prevTime = currTime;

        /* handle events. */
        while (enet_host_service(client, &event, 0) > 0) {
            switch (event.type)
            {
            case ENET_EVENT_TYPE_NONE:
                break;
            case ENET_EVENT_TYPE_CONNECT:
                handle_connect(event);
                break;
            case ENET_EVENT_TYPE_RECEIVE:
                handle_receive(event);
                break;
            case ENET_EVENT_TYPE_DISCONNECT:
                terminate = !handle_disconnect(event);
            }
        }

        if (terminate) break;

        /* update game logic as lag permits */
        while (lag >= Common::MS_PER_UPDATE) {
            lag -= Common::MS_PER_UPDATE;
            update(); // update at a fixed rate each time
        }

        while (lagExport >= Common::MS_PER_STATISTICS_EXPORT) {
            lagExport -= Common::MS_PER_STATISTICS_EXPORT;
            export_csv();
        }
    }

    log_statistics();

    enet_host_destroy(client);
}