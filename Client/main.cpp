#include <iostream>
#include <enet/enet.h>
#include "client.h"

int main()
{
    if (enet_initialize() != 0)
    {
        LOG_ERROR("An error occurred while initializing ENet.\n");
        return EXIT_FAILURE;
    }
    atexit(enet_deinitialize);

    Client client;
    client.run();
}
