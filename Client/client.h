#pragma once

#include <enet/enet.h>
#include <cstdint>
#include <string>
#include "common.h"

class Client
{
private:
	enum class ClientStatus {
		Active,
		ConnectedToEntryServer,
		RedirectInitiated,
		ConnectedToServer
	};

	ClientStatus clientStatus = ClientStatus::Active;
	EntityState playerState;
	ENetAddress serverAddress;
	ENetHost* client;
	ENetPeer* peer;
	uint32_t updateCount = 0;
	std::string csvPath;

	void handle_connect(const ENetEvent& event);
	bool handle_disconnect(const ENetEvent& event); // returns false if no redirect was initiated, used to terminate simulated client in tests
	void handle_receive(const ENetEvent& event);
	void update();
	void log_statistics();
	void export_csv();
public:
	void run();
};

